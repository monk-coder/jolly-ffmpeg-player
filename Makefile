
bin=jolly-player
FFMPEG_LIBS=libavutil libavformat libswscale libavcodec 
LDFLAGS=`pkg-config --libs $(FFMPEG_LIBS)` -lSDL -g
CFLAGS=`pkg-config --cflags $(FFMPEG_LIBS)` -g
objs=main.o

$(bin) : $(objs)
	gcc -o $(bin) $(objs) $(LDFLAGS)

%.o:%.c
	gcc $(CFLAGS) -c $<

clean:
	-rm *.o $(bin)
