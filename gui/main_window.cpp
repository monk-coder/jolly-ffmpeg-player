
#include <QApplication>
#include <QWidget>
#include <QTextStream>
#include <QDesktopWidget>
#include "movie-box.hpp"

int main(int argc, char *argv[])
{
    const int WIN_WIDTH = 640;
    const int WIN_HEIGHT = 480;
    int screenWidth, screenHeight;
    int posX, posY;

    QApplication app(argc, argv);

    QDesktopWidget *desktop = QApplication::desktop();
    screenWidth = desktop->width();
    screenHeight = desktop->height();

    posX = (screenWidth - WIN_WIDTH) /2 ;
    posY = (screenHeight - WIN_HEIGHT) /2;
    
    MovieBox * window = new MovieBox();
    window->setAttribute(Qt::WA_DeleteOnClose);
    window->resize(WIN_WIDTH, WIN_HEIGHT);
    window->move(posX, posY);
    window->setWindowTitle("Hello, QT!");
    window->show();
    return app.exec();
}
