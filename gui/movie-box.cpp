
#include "movie-box.hpp"
#include <QLabel>
#include <QPixmap>
#include <QVBoxLayout>
#include <QPushButton>
#include <QProgressBar>

MovieBox::MovieBox(QWidget *parent)
{
    QVBoxLayout * layout = new QVBoxLayout(this);
    this->setLayout(layout);

    QVBoxLayout * topMain = new QVBoxLayout(this);
    layout->addLayout(topMain);
    // QPixmap pixmap("emacs-qt.png");
    QLabel *label = new QLabel(this);   // must set the label's parent with this, otherwise it won't display
    // label->setPixmap(pixmap);
    // label->setGeometry(5, 10, 590, 500);
    topMain->addWidget(label);

    QHBoxLayout *bottomBar = new QHBoxLayout(this);
    layout->addLayout(bottomBar);

    QPushButton * playButton = new QPushButton("Play", this);
    QPushButton * fullscreenButton = new QPushButton("full screen", this);
    QProgressBar * progressBar = new QProgressBar(this);
    bottomBar->addWidget(playButton);
    bottomBar->addWidget(progressBar);
    bottomBar->addWidget(fullscreenButton);
}

MovieBox::~MovieBox()
{
}
