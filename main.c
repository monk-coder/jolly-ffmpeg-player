
/*
 * you need ffmpeg development files
 * apt-get install ffmpeg; pacman -S ffmpeg; yum install ffmpeg; pkg install ffmpeg
 */
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <SDL/SDL.h>
#include <SDL/SDL_thread.h>
#include <stdio.h>
#include <string.h>

SDL_Overlay * init_sdl(int width, int height, const char* title)
{
    if(SDL_Init(SDL_INIT_VIDEO| SDL_INIT_AUDIO |SDL_INIT_TIMER)) {
        fprintf(stderr, "Could not initialize SDL - %s\n", SDL_GetError());
        exit(1);
    }

    atexit(SDL_Quit);

    SDL_Surface *screen;
    screen = SDL_SetVideoMode(width, height, 0, 0);
    if(!screen) {
        fprintf(stderr, "Could not  SDL - %s\n", SDL_GetError());
        exit(1);
    }

    char title_str[256] = "jolly-player@";
    strcat(title_str, title);
    SDL_WM_SetCaption(title_str, NULL);

    SDL_Overlay *bmp;
    bmp = SDL_CreateYUVOverlay(width, height, SDL_YV12_OVERLAY, screen);
    return bmp;
}

int main(int argc, char *argv[])
{
    if (argc < 2) {
        printf("Usage: %s <filepath>!\n", argv[0]);
        return 0;
    }

    /* register all available file formats and codecs */
    av_register_all();

    AVFormatContext *p_format_context = NULL;

    /* argv[1] contains the file name;
     * The last 2 args are used to specify the fileformat, and format options
     * Setting them to NULL, libavformat will auto-detect them. */
    if(avformat_open_input(&p_format_context, argv[1], NULL, NULL)!=0)
        return -1;

    /* Retrive stream information */
    /* `av_find_stream_info' is deprecated  */
    if(avformat_find_stream_info(p_format_context, NULL) < 0)
        return -1;              /* could not find stream information */

    printf("There are %d streams in %s\n", p_format_context->nb_streams, argv[1]);

    int i;
    int video_stream_id = -1;
    int audio_stream_id = -1;
    for(i=0; i < p_format_context->nb_streams; i++) {
        if(p_format_context->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO &&
            video_stream_id < 0) {
            video_stream_id = i;
        }
        if(p_format_context->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO &&
            audio_stream_id < 0) {
            audio_stream_id = i;
        }
    }

    if(video_stream_id < 0 && audio_stream_id < 0){
        printf("I could not find any video stream or audio stream! I will exit now!\n");
        return -1;
    }

    if(video_stream_id >= 0)
        printf("Find a video stream: %d\n", video_stream_id);

    if(audio_stream_id >= 0)
        printf("Find a audio stream: %d\n", audio_stream_id);

    AVCodecContext *video_codec_context = NULL;
    video_codec_context = p_format_context->streams[video_stream_id]->codec;

    AVCodecContext *audio_codec_context = NULL;
    audio_codec_context = p_format_context->streams[audio_stream_id]->codec;

    int video_codec_id, audio_codec_id;
    video_codec_id = video_codec_context->codec_id;
    printf("Found the video codec id: %d\n", video_codec_id);
    audio_codec_id = audio_codec_context->codec_id;
    printf("Found the audio codec id: %d\n", audio_codec_id);

    SDL_AudioSpec wanted_spec, spec;

    AVCodec *p_vcodec;
    p_vcodec = avcodec_find_decoder(video_codec_id);
    if(p_vcodec == NULL) {
        printf("Shit, Unsupported video codec!\n");
        return -1;
    }

    /* Open codec */
    /* Make sure to init the p_dict to NULL, IMPORTANT */
    AVDictionary *p_dict = NULL;
    if(avcodec_open2(video_codec_context, p_vcodec, &p_dict) < 0){
        avformat_close_input(&p_format_context);
        printf("WTF, I could not open the codec!\n");
        return -1;
    }

    AVFrame *p_frame = NULL;
    /* The p_frame must be freed using avcodec_free_frame() */
    p_frame = avcodec_alloc_frame();
    if(!p_frame) {
        avformat_close_input(&p_format_context);
        printf("WTF, avcodec_alloc_frame failed!\n");
        return -1;
    }

    /* uint8_t *buffer; */
    /* int num_bytes; */
    /* num_bytes = avpicture_get_size(PIX_FMT_RGB24, video_codec_context->width, video_codec_context->height); */
    /* printf("Oh, A picture size is %d bytes\n", num_bytes); */

    /* buffer = (uint8_t *)av_malloc(num_bytes * sizeof(uint8_t)); */
    /* avpicture_fill((AVPicture *)p_frameRGB, buffer, PIX_FMT_RGB24, */
    /*         video_codec_context->width, video_codec_context->height); */

    /* PPM = Portable Pixel Map. */
    int frame_finished = 0;

    printf("size: %dx%d, format: %d\n", video_codec_context->width,
            video_codec_context->height, video_codec_context->pix_fmt);

    struct SwsContext *sws_context = NULL;

    /* struct SwsContext* sws_getContext(int srcW, int srcH, int srcFormat, */
    /*         int dstW, int dstH, int dstFormat, int flags, */
    /*         SwsFilter *srcFilter, SwsFilter *dstFilter, double *param) */
    /* Returns an SwsContext to be used in sws_scale.  */

    /* sws_getContext will result in segmentfault, Why? */
    sws_context = sws_getContext (
        video_codec_context->width,
        video_codec_context->height,
        video_codec_context->pix_fmt,
        video_codec_context->width,
        video_codec_context->height,
        PIX_FMT_YUV420P,
        SWS_BILINEAR,
        NULL,
        NULL,
        NULL
        );

    SDL_Overlay * bmp;
    bmp = init_sdl(video_codec_context->width, video_codec_context->height, argv[1]);

    AVPacket packet;
    SDL_Event event;

    while(av_read_frame(p_format_context, &packet) >= 0) {
        if(packet.stream_index == video_stream_id) {
            avcodec_decode_video2(video_codec_context, p_frame, &frame_finished, &packet);
            if(frame_finished){
                SDL_LockYUVOverlay(bmp);
                AVPicture picture;
                picture.data[0] = bmp->pixels[0];
                picture.data[1] = bmp->pixels[2];
                picture.data[2] = bmp->pixels[1];

                picture.linesize[0] = bmp->pitches[0];
                picture.linesize[1] = bmp->pitches[2];
                picture.linesize[2] = bmp->pitches[1];

                // Convert the image from its native format to RGB
                sws_scale(sws_context,
                        (uint8_t const * const *)p_frame->data,
                        p_frame->linesize,
                        0,
                        video_codec_context->height,
                        picture.data,
                        picture.linesize
                    );

                SDL_UnlockYUVOverlay(bmp);

                SDL_Rect rect;
                rect.x = 0;
                rect.y = 0;
                rect.w = video_codec_context->width;
                rect.h = video_codec_context->height;
                SDL_DisplayYUVOverlay(bmp, &rect);
                SDL_Delay(20);
            }
        }
        
        av_free_packet(&packet);
        SDL_PollEvent(&event);
        if(event.type == SDL_QUIT){
            SDL_Quit();
            break;
        }
    }

    // Free the RGB image
    /* av_free(buffer); */
    /* av_free(p_frameRGB); */
  
    // Free the YUV frame
    if(p_frame)
        avcodec_free_frame(&p_frame);
  
    // Close the codec
    avcodec_close(video_codec_context);
    avformat_close_input(&p_format_context);
    return 0;
}
